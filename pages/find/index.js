// pages/find/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    clientHeight: 0,
    tagScrollViewHeigh: 60,
    contentScrollViewHeight: 0,
    tagViewItemHeight : 0,
    tagViewHeight : 0,
    scrollHeight : 0,
    scrollTop : 0,

    autoPlay : true,

    checkedtags: [
    ],

    tags: [
      {
        num:1,
        text: "机器学习",
        checked: true
      },
      {
        num: 2,
        text: "仿真",
        checked: true
      },
      {
        num: 3,
        text: "游戏",
        checked: true
      },
      {
        num: 4,
        text: "ocr",
        checked: true
      },
      {
        num: 5,
        text: "文字",
        checked: false
      },
      {
        num: 6,
        text: "区块链",
        checked: false
      },
      {
        num: 7,
        text: "图标",
        checked: false
      },
      {
        num: 8,
        text: "教育",
        checked: false
      },
      {
        num: 9,
        text: "三维",
        checked: false
      },
      {
        num: 10,
        text: "字幕",
        checked: false
      },
      {
        num: 11,
        text: "文字",
        checked: false
      },
      {
        num: 12,
        text: "区块链",
        checked: false
      },
      {
        num: 13,
        text: "游戏",
        checked: true
      },
      {
        num: 14,
        text: "ocr",
        checked: true
      },
      {
        num: 15,
        text: "文字",
        checked: false
      },
      {
        num: 16,
        text: "区块链",
        checked: false
      },
      {
        num: 17,
        text: "图标",
        checked: false
      },
      {
        num: 18,
        text: "教育",
        checked: false
      },
      {
        num: 19,
        text: "三维",
        checked: false
      },
    ],

    cats : [
      {
        name: "应用类型",
        item: [
          { num: 1, text: "游戏", checked: false },
          { num: 2, text: "APP", checked: false },
          { num: 3, text: "小程序", checked: false },
          { num: 4, text: "H5应用", checked: false },
          { num: 5, text: "快应用", checked: false },
          { num: 6, text: "小游戏", checked: false }
        ]
      },
      {
        name : "交互方式",
        item: [
            { num: 1, text: "眼球追踪", checked : false },
            { num: 2, text: "动作捕捉", checked: false },
            { num: 3, text: "肌电模拟", checked: false },
            { num: 4, text: "触觉反馈", checked: false },
            { num: 5, text: "语音", checked: false },
            { num: 6, text: "方向追踪", checked: false },
            { num: 7, text: "手势追踪", checked: false },
            { num: 8, text: "传感器", checked: false },
          ]
      },
      {
        name: "新技术",
        item: [
          { num: 1, text: "区块链", checked: false },
          { num: 2, text: "人工智能", checked: false },
          { num: 3, text: "机器学习", checked: false },
          { num: 4, text: "仿真", checked: false },
          { num: 5, text: "大数据", checked: false },
          { num: 6, text: "物联网", checked: false },
          { num: 7, text: "云计算", checked: false },
          { num: 8, text: "共享经济", checked: false },
        ],
      },
      {
        name: "互联网+",
        item: [
          { num: 1, text: "通信", checked: false },
          { num: 2, text: "购物", checked: false },
          { num: 3, text: "饮食", checked: false },
          { num: 4, text: "交易", checked: false },
          { num: 5, text: "金融", checked: false },
          { num: 6, text: "教育", checked: false },
        ],
      },
      {
        name: "建筑概念",
        item: [
          { num: 1, text: "历史组合", checked: false },
          { num: 2, text: "人与社会", checked: false },
          { num: 3, text: "神性", checked: false },
          { num: 4, text: "因地制宜", checked: false },
          { num: 5, text: "立体城市", checked: false },
          { num: 6, text: "城市理想", checked: false },
          { num: 7, text: "建筑功能", checked: false },
        ],
      }

    ],

    catIndex : 0

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    var clientHeight = wx.getSystemInfoSync().windowHeight
    var tagScrollViewHeigh = this.data.tagScrollViewHeigh
    var contentScrollViewHeight = clientHeight - tagScrollViewHeigh
    var tagViewItemHeight = (contentScrollViewHeight * 0.9) / 8
    var tagViewHeight = (contentScrollViewHeight * 0.9 )
    var scrollHeight = tagViewItemHeight * this.data.tags.length
    this.setData({
      clientHeight: clientHeight,
      tagScrollViewHeigh: tagScrollViewHeigh,
      contentScrollViewHeight: contentScrollViewHeight,
      tagViewItemHeight: tagViewItemHeight,
      tagViewHeight: tagViewHeight,
      scrollHeight: scrollHeight
    });
  
  },

  onScroll : function(e) {
    var scrollHeight = e.detail.scrollHeight
    var scrollTop = e.detail.scrollTop

    this.setData({
      scrollTop: scrollTop,
      scrollHeight: scrollHeight
    })


  },

  changeSwiper: function(e) {

    //console.log(e)

    this.data.catIndex = e.detail.current

    var autoPlay ;

    if(e.detail.source === "autoplay" ) {
      autoPlay = true;
    } else {
      autoPlay = false;
    }

    this.setData({
      catIndex: e.detail.current,
      autoPlay: autoPlay
    })
  },

  tagChange : function(e) {
    //console.log('tagChange发生change事件，携带value值为：', e.detail.value)
    var items = this.data.cats[this.data.catIndex].item;
    var checkArr = e.detail.value;
    var cats = this.data.cats;
    var newCheckedTag = new Array();

    for (var i = 0; i < items.length; i++) {
      items[i].checked = false
    }

    for (var i = 0; i < items.length; i++) {
      for (var j = 0; j < checkArr.length; j++) {
        if (checkArr[j] === items[i].text) {
          items[i].checked = true
        }
      }
    }

    cats[this.data.catIndex].item = items

    for(var i = 0;i < cats.length; ++i) {
      var item = cats[i].item;
      for(var j = 0;j < item.length; ++j) {
        if(item[j].checked) {
          console.log(item[j])
          newCheckedTag.push(item[j])
        }
      }
    }

    //console.log(newCheckedTag)

    this.setData({
      cats: cats,
      autoPlay:false,
      checkedtags: newCheckedTag,
    })

  },

  checkedTagChange: function (e) {

    //console.log('checkedTagChange发生change事件，携带value值为：', e.detail.value)

    var checkedtags = this.data.checkedtags
    var checkArr = e.detail.value;
    var cats = this.data.cats;

    var newCheckedTag = new Array();

    for (var i = 0; i < checkedtags.length; i++) {
      for (var j = 0; j < checkArr.length; j++) {
        if (checkArr[j] === checkedtags[i].text) {
          newCheckedTag.push(checkedtags[i])
        }
      }
    }

    for (var i = 0; i < cats.length; ++i) {
      for (var j = 0; j < cats[i].item.length; ++j) {

        var checked = false
        for(var k = 0; k < newCheckedTag.length; ++k) {
          if (cats[i].item[j].text === newCheckedTag[k].text) {
            checked = true;
          }
        }
        
        cats[i].item[j].checked = checked;
        
      }
    }

    this.setData({
      checkedtags: newCheckedTag,
      cats : cats
    })


  },

  onSearch : function(e) {

    console.log(e)
    wx.navigateTo({
      url: "../impact/index?checkedTags=" + JSON.stringify(this.data.checkedtags)
    })

  },

  gotoSearch: function (e) {

    console.log(e)
    wx.navigateTo({
      url: "../search/index"
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    var clientHeight = wx.getSystemInfoSync().windowHeight
    var tagScrollViewHeigh = this.data.tagScrollViewHeigh
    var contentScrollViewHeight = clientHeight - tagScrollViewHeigh
    var tagViewItemHeight = (contentScrollViewHeight * 0.9) / 8
    var tagViewHeight = (contentScrollViewHeight * 0.9)
    var scrollHeight = tagViewItemHeight * this.data.tags.length
    this.setData({
      clientHeight: clientHeight,
      tagScrollViewHeigh: tagScrollViewHeigh,
      contentScrollViewHeight: contentScrollViewHeight,
      tagViewItemHeight: tagViewItemHeight,
      tagViewHeight: tagViewHeight,
      scrollHeight: scrollHeight
    });
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },



})