// pages/find/index.js
import utils from '../../utils/index'

Page({

  /**
   * 页面的初始数据
   */
  data: {

    clientHeight: 0,
    tagScrollViewHeigh: 50,
    contentScrollViewHeight: 0,

    tags: [
    ],

    keyworld:"",


    seletedTags: [
    ],

    impactArticleList: [

    ],

    hasmore: false,
    limit: 10,
    offset: 0
  },



  onScrolltolower: function () {

    if (this.data.hasmore) {
      this.getMoreArticleList()
    }

  },

  getMoreArticleList: function () {

    wx.showNavigationBarLoading()

    utils.getSearchArticleList(this.data.keyworld, this.data.limit, this.data.offset, (res) => {

      console.log(res)

      var limit = res.data.meta.limit
      var total_count = res.data.meta.total_count
      var offset = res.data.meta.offset
      var hasmore = total_count > (offset + limit)

      console.log(hasmore)

      var impactArticleList = this.data.impactArticleList

      impactArticleList = impactArticleList.concat(res.data.objects)

      this.setData({
        impactArticleList: impactArticleList,
        hasmore: hasmore,
        offset: offset + limit
      });

      wx.hideNavigationBarLoading()
    })
  },

  getSearchArticleList: function () {

    this.data.impactArticleList = []
    this.data.offset  = 0

    wx.showNavigationBarLoading()

    utils.getSearchArticleList(this.data.keyworld, this.data.limit, this.data.offset, (res) => {

      console.log(res)

      var limit = res.data.meta.limit
      var total_count = res.data.meta.total_count
      var offset = res.data.meta.offset
      var hasmore = total_count > (offset + limit)

      console.log(hasmore)

      var impactArticleList = this.data.impactArticleList

      impactArticleList = impactArticleList.concat(res.data.objects)

      this.setData({
        impactArticleList: impactArticleList,
        hasmore: hasmore,
        offset: offset + limit
      });

      wx.hideNavigationBarLoading()
    })
  },



  onTapArticle: function (e) {
    wx.navigateTo({
      url: "../impactread/index?article_id=" + e.currentTarget.dataset.article_id
    })
  },

  onInput: function(e) {

    var keyworld = e.detail.value

    this.data.keyworld = keyworld

    if(keyworld != "") {
      this.getSearchArticleList()
    }

    

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {


    var clientHeight = wx.getSystemInfoSync().windowHeight
    var tagScrollViewHeigh = this.data.tagScrollViewHeigh
    var contentScrollViewHeight = clientHeight - tagScrollViewHeigh



    this.setData({
      clientHeight: clientHeight,
      tagScrollViewHeigh: tagScrollViewHeigh,
      contentScrollViewHeight: contentScrollViewHeight,
    });


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    wx.getSystemInfo(res => {
      // todo
      var clientHeight = res.windowHeight
      var contentScrollViewHeight = clientHeight - this.data.tagScrollViewHeigh

      this.setData({
        clientHeight: clientHeight,
        contentScrollViewHeight: contentScrollViewHeight
      });
    }
    );
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {


  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})