// pages/record/index.js
import utils from '../../utils/index'

const initBottomHeight = 0
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {

    border_debug_: "border: 1rpx solid #746e6e;",

    isRecord : true,

    bottomHeight: initBottomHeight,

    updateArticle : false,

    editRowNum : -1,

    article: {
      title: "",
      intro: "",
      content: [
        { rownum: 1, label: "normal", content: "", imageurl: "none" ,focus: false}
      ]
    },
    curRecordId: null
  },

  onTitleInput : function(e) {

   this.data.article.title = e.detail.value
   this.data.updateArticle = true

  },

  onImageTextInput : function(e) {

    this.data.updateArticle = true

  },

  /*
   * 监听文本框输入变化
   */
  onInput : function(e) {
    
    console.log(e);
    var content = this.data.article.content;
    var article = this.data.article;
    var rownum = e.target.id;
    var value = e.detail.value;
    var s = value.charAt(value.length-1);

    if(s == "`") { // 输入回车键
      console.log("enter");

      var newrow = { rownum: Number.parseInt(rownum) + 1, label: "normal", content: "", imageurl: "none", focus: false };

      var oldrow = { rownum: Number.parseInt(rownum) , label: "normal", content: "", imageurl: "none",focus:false };

      

      content.splice(rownum, 0, oldrow);

      for (var i = 0; i < content.length; ++i) {
        content[i].focus = false;
      }

      content[rownum].focus = true;


      for (var i = rownum; i < content.length; ++i) {
        content[i].rownum = content[i].rownum + 1;
      }

    }else {
      for(var i = 0; i < content.length; ++i) {
        if(content[i].rownum == rownum) {
          content[i].content = value;
        }
      }
    }

    article.content = content;

    this.data.updateArticle = true
    this.data.article = article
    this.editRowNum = rownum

    // this.setData ({
    //   article: article,
    //   editRowNum : rownum
    // });

  },
  

  /*
   * 监听文本框失去焦点
   */
  onBlur : function(e) {
    console.log(e)
    
    var article = this.data.article;
    var rownum = e.target.id;

   // article.content[Number.parseInt(rownum) - 1].focus = false;

    this.setData({
      article: article
    });

    this.editBlur(e)
  },

  /*
   * 监听文本框获得焦点
   */
  onFocus : function(e) {
    console.log(e)
    var content = this.data.article.content;
    var article = this.data.article;
    var rownum = e.target.id;
   
   // content[Number.parseInt(rownum) - 1].focus = true;

    article.content = content;

    this.data.article.content = content;

    this.setData({
      article: article
    });

    this.editFocus(e)
  },

  editFocus: function (e) {

    var height  = e.detail.height

    if (!isNaN(height)) {
      this.setData({
        bottomHeight: height + initBottomHeight
      })
    }
  },

  /**
   * 输入框失焦
   */
  editBlur: function () {
    this.setData({
      bottomHeight: initBottomHeight
    })
  },

  tapImage : function(e) {

    console.log(e)

    var article = this.data.article
    var content = this.data.article.content

    var rownum = e.currentTarget.dataset.rownum

    content[rownum - 1].focus = !content[rownum - 1].focus

    article.content = content

    this.setData({
      article : article
    })

  },

  /**
   * 添加图片
   */
  addImage : function(e) {

    var content = this.data.article.content;
    var article = this.data.article;

    wx.chooseImage({

      success: res => {
        let MyFile = new wx.BaaS.File()
        let fileParams = { filePath: res.tempFilePaths[0] }
        let metaData = { categoryName: 'image' }

        MyFile.upload(fileParams, metaData).then(res => {
          /*
           * 注: 只要是服务器有响应的情况都会进入 success, 即便是 4xx，5xx 都会进入这里
           * 如果上传成功则会返回资源远程地址,如果上传失败则会返回失败信息
           */

          let data = res.data  // res.data 为 Object 类型


          var row = { rownum: content.length + 1, label: "image", content: "", imageurl: data.path, focus: false }

          content.push(row)

          row = { rownum: content.length + 1, label: "normal", content: "", imageurl: data.path, focus: true }

          content.push(row)

          article.content = content

          this.data.updateArticle = true

          this.setData({
            article: article
          }

          )
        }, err => {

        })
      }
    })

  },

  /**
   * 删除图片
   */
  deleteImage : function(e) {
    var article = this.data.article
    var content = this.data.article.content

    var rownum = e.currentTarget.dataset.rownum

    content.splice(rownum - 1,1)

    for(var i = 0; i < content.length; ++i) {
      content[i].rownum = i + 1;
    }

    article.content = content

    this.setData({
      article : article,
      updateArticle : true
    })

  },


  onViewTap : function(e) {
    console.log(e)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    var isRecord;

    if (options.isRecord === "true") {
      isRecord = true;
    } else if (options.isRecord === "false") {
      isRecord = false;
    }

    this.data.isRecord = isRecord

    console.log(options)
    console.log(isRecord)

    if (isRecord) {
      if (options.article_id == null) {
        utils.addArticle(this, (res) => {
          this.setData({
            curRecordId: res.data.id
          });
        })

      } else {

        utils.getArticleEdit(options.article_id, (res) => {
          var article = JSON.parse(res.data.content)

          console.log(article)

          this.setData({
            article: article
          })
        })

        this.setData({
          curRecordId: options.article_id
        });

      }
    } else {

      utils.getArticleForkEdit(options.article_id, (res) => {
        var article = JSON.parse(res.data.content)

        console.log(article)

        this.setData({
          article: article
        })
      })

      this.setData({
        curRecordId: options.article_id
      });

    }
    
  },

  updateArticle : function() {

    if(this.data.isRecord) {
      if (this.data.updateArticle == true) {
        utils.updateArticle(this, (res) => {
          console.log(res)
        })
      }
    } else {

      if (this.data.updateArticle == true) {
        utils.updataArticleFork(this, (res) => {
          console.log(res)
        })
      }
      
    }
    

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

    this.updateArticle()

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

    this.updateArticle()
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})