
// pages/impactread/index.js
var util = require('../../utils/util.js')
import utils from '../../utils/index'


Page({

  /**
   * 页面的初始数据
   */
  data: {

    article_id: null,
    article: {},
    forkFromYouArticleList:[],
    author: {},
    isZan: false,
    isFork: false,
    stars: 0,
    forks: 0

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    this.getImpactArticle(options.article_id)
    this.getForkFromYouArticleList(options.article_id)

  },

  getForkFromYouArticleList:function(article_id) {

    utils.getForkFromYouArticleList(article_id,(res) => {
      
      var forkFromYouArticleList = res.data.objects

      for(var i = 0; i < forkFromYouArticleList.length;++i) {
        forkFromYouArticleList[i].content = JSON.parse(forkFromYouArticleList[i].content).content
      }

      this.setData({
        forkFromYouArticleList: forkFromYouArticleList
      })

    })

  },


  getImpactArticle: function (article_id) {

    utils.getArticleRead(article_id, (res) => {

      this.setData({
        article: JSON.parse(res.data.content),
        author: {
          author_avatar: res.data.created_by.avatar,
          author_name: res.data.created_by.nickname,
          updated_time: res.data.updated_time
        },
        article_id: res.data.id
      })

    })

  },

  onDelete : function(e) {
    
    utils.deletePusblishArticle(this.data.article_id, (success) => {

      wx.navigateBack(1)
      util.showSuccess("删除成功")

    } , (err) => {

    } ) 

  },

  onTop : function(e) {

    var article_id = e.currentTarget.id

    utils.setForkFromYouArticleTop(article_id,(success) => {
      console.log(success)
        util.showSuccess("置顶成功")
        this.getForkFromYouArticleList(options.article_id)
    
    },(err) => {
      console.error(err)
      util.showModel("置顶失败")
    })

  },

 

  // 预览图片
  previewImg: function (e) {
    wx.previewImage({
      current: e.target.dataset.url,
      urls: [e.target.dataset.url]
    })
  },


})