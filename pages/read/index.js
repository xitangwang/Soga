// pages/record/index.js
var util = require('../../utils/util.js')
import utils from '../../utils/index'

Page({

  /**
   * 页面的初始数据
   */
  data: {

    border_debug_: "border: 0rpx solid #746e6e;",

    article : { }
  },

  /**
   * 生命周期函数--监听页面加载
   */

  onLoad: function (options) {
    
    utils.getArticleRead(options.article_id,(res) => {
      
      var article = JSON.parse(res.data.content)

      this.setData ({
        article : article
      })

    })

  },

 

  // 预览图片
  previewImg: function (e) {
    console.log(e)
    wx.previewImage({
      current: e.target.dataset.url,
      urls: [e.target.dataset.url]
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})