// pages/find/index.js
import utils from '../../utils/index'

Page({

  /**
   * 页面的初始数据
   */
  data: {

    clientHeight: 0,
    tagScrollViewHeigh: 60,
    contentScrollViewHeight: 0,

    tags: [
    ],


    seletedTags : [
    ],

    impactArticleList : [

    ],

    hasmore : false,
    limit : 10,
    offset : 0
  },

  tapTag:function () {
    
  },

  checkboxChange: function (e) {

    console.log('checkbox发生change事件，携带value值为：', e.detail.value)

    var items = this.data.tags;
    var checkArr = e.detail.value;

    console.log(items)
    console.log(checkArr)

    for (var i = 0; i < items.length; i++) {
          items[i].checked = false
    }

    for (var i = 0; i < items.length; i++) {
        for(var j = 0; j < checkArr.length; j++) {
          if(checkArr[j] === items[i].text) {
            items[i].checked = true
          }
        }
    }

    this.setData({
      tags: items,
      offset : 0,
      seletedTags : checkArr,
      impactArticleList : []
    })

    this.getImpactArticleList()

  },

  getImpactArticleList : function() {

    wx.showNavigationBarLoading()

    utils.getImpactArticleList(this.data.seletedTags,this.data.limit,this.data.offset,(res) => {

      console.log(res)

      var limit = res.data.meta.limit
      var total_count = res.data.meta.total_count
      var offset = res.data.meta.offset
      var hasmore = total_count > (offset + limit)

      console.log(hasmore)

      var impactArticleList = this.data.impactArticleList
      
      impactArticleList = impactArticleList.concat(res.data.objects)

      this.setData({
        impactArticleList: impactArticleList,
        hasmore: hasmore,
        offset : offset + limit
      });

      wx.hideNavigationBarLoading()
    })
  },

  onScrolltolower : function() {

    if(this.data.hasmore) {
      this.getImpactArticleList()
    }

  },


  onTapArticle: function (e) {
    console.log(e)
      wx.navigateTo({
        url: "../impactread/index?article_id=" + e.currentTarget.dataset.article_id
      })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    var checkedTags = JSON.parse(options.checkedTags)
    var seletedTags = new Array()

    for(var i = 0;i < checkedTags.length; ++ i) {
      if(checkedTags[i].checked) {
        seletedTags.push(checkedTags[i].text)
      }
    }

    var clientHeight = wx.getSystemInfoSync().windowHeight
    var tagScrollViewHeigh = this.data.tagScrollViewHeigh
    var contentScrollViewHeight = clientHeight - tagScrollViewHeigh

    this.data.seletedTags = seletedTags

    this.getImpactArticleList()

    this.setData({
      clientHeight: clientHeight,
      tagScrollViewHeigh: tagScrollViewHeigh,
      contentScrollViewHeight: contentScrollViewHeight,
      tags: checkedTags,
      seletedTags: seletedTags
    });
    

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    wx.getSystemInfo(res => {
        // todo
        var clientHeight = res.windowHeight
        var contentScrollViewHeight = clientHeight - this.data.tagScrollViewHeigh

        this.setData({
          clientHeight: clientHeight,
          contentScrollViewHeight: contentScrollViewHeight
        });
      }
    );
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})