//app.js
const utils = require('./utils/util.js')

App({
  onLaunch: function () {
    let that = this

    // 引入 BaaS SDK
    require('./utils/sdk-v1.4.0')

    let clientId = this.globalData.clientId

    wx.BaaS.init(clientId)

    wx.getStorage({
      key: 'history',
      success: (res) => {
        this.globalData.history = res.data
      },
      fail: (res) => {
        console.log("get storage failed")
        console.log(res)
        this.globalData.history = []
      }
    })
  },

  // 权限询问
  getRecordAuth: function () {
    wx.getSetting({
      success(res) {
        console.log("succ")
        console.log(res)
        if (!res.authSetting['scope.record']) {
          wx.authorize({
            scope: 'scope.record',
            success() {
              // 用户已经同意小程序使用录音功能，后续调用 wx.startRecord 接口不会弹窗询问
              console.log("succ auth")
            }, fail() {
              console.log("fail auth")
            }
          })
        } else {
          console.log("record has been authed")
        }
      }, fail(res) {
        console.log("fail")
        console.log(res)
      }
    })
  },

  onHide: function () {
    wx.stopBackgroundAudio()
  },

  globalData: {
    clientId: 'eda8682163418a991ac3', // 从 BaaS 后台获取 ClientID
    private_article_tableId: 38804, 
    public_article_tableId: 38884,
    fork_article_tableId: 38885,
    tag_tableId: 38877,
    catalogue_tableId: 38883,
    public_article_tag_tableId: 38912,
    fork_tableId: 38881,
    star_tableId:38880,
    
    history: [],
  }
})
