// pages/impactread/index.js
var util = require('../../utils/util.js')
import utils from '../../utils/index'


Page({

  /**
   * 页面的初始数据
   */
  data: {

    article_id : null,
    article: {},
    author : {},
    isZan : false,
    isFork :  false,
    stars : 0,
    forks : 0,
    canDoZan:true,
    canDoFork:true

  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    this.getImpactArticle(options.article_id)
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  onZan : function() {

    if (this.data.canDoZan)
    {
      if (this.data.isZan) {
        this.unZan(this.data.article_id)
      } else {
        this.zan(this.data.article_id)
      }
    }
  },

  onFork : function() {
    if (this.data.canDoFork)
    {
      if (this.data.isFork) {
        this.unFork(this.data.article_id)
      } else {
        this.fork(this.data.article_id)
      }
    }
  },

  getImpactArticle : function(article_id) {

    utils.getArticleRead(article_id,(res) => {

      console.log(res.data.stars)

      this.setData({
        article: JSON.parse(res.data.content),
        author : {
          author_avatar: res.data.created_by.avatar,
          author_name: res.data.created_by.nickname,
          updated_time: res.data.updated_time},
        stars: res.data.stars,
        forks: res.data.forks,
        article_id : res.data.id
      })

    })

    this.isZan(article_id,this.data.stars)
    this.isFork(article_id)

  },

  zan : function(article_id) {

    this.data.canDoZan = false

    utils.zan(article_id, (res) => {
      this.data.canDoZan = true

      this.setData({
        isZan: true,
        stars : this.data.stars + 1
      })

    })

  },

  unZan: function (article_id) {

    this.data.canDoZan = false

    utils.unZan(article_id, (res) => {

      this.data.canDoZan = true

      this.setData({
        isZan: false,
        stars: this.data.stars - 1
      })

    })

  },

  fork: function (article_id) {

    this.data.canDoFork = false

    utils.fork(article_id, (res) => {

      this.data.canDoFork = true

      this.setData({
        isFork: true,
        forks: this.data.forks + 1
      })

    })
  },

  unFork : function(article_id) {

    this.data.canDoFork = false

    utils.unFork(article_id, (res) => {

      this.data.canDoFork = true

      this.setData({
        isFork: false,
        forks: this.data.forks - 1
      })

    })
  },

  isZan : function(article_id) {

    utils.isZan(article_id, (res) => {

      this.setData({
        isZan : res.data.objects.length > 0
      })

    })

  },

  isFork : function(article_id) {

    utils.isFork(article_id, (res) => {

      this.setData({
        isFork: res.data.objects.length > 0
      })

    })

  },

  // 预览图片
  previewImg: function (e) {
    wx.previewImage({
      current: e.target.dataset.url,
      urls: [e.target.dataset.url]
    })
  },


})