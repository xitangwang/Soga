import { language } from '../../utils/conf.js'

let button = {}

// 按钮配置
language.forEach(item=>{
  button = {
    buttonText: "",
    lang: item.lang_content,
    lto: item.lang_to[0],
    msg: item.hold_talk,
    buttonType: 'normal',
  }
})

// // 按钮配置
// let button = {
//   buttonText: "中文",
//   lang: "zh_CN",
//   lto: "en_US",
//   msg: "长按说话",
//   buttonType: 'normal',
// }

// 按钮对应图片
let buttonBackground = {
  zh_CN: {
    normal: '../../image/btn_add.png',
    press: '../../image/shengbo.gif',
    disabled: '../../image/btn_ok.png',
  }
}

Component({
  properties: {
    buttonDisabled: {
      type: Boolean,
      value: false,
      observer: function(newVal, oldVal){
        let buttonType = newVal ? 'disabled' : 'normal'
        this.changeButtonType(buttonType)

      }
    },
  },

  data: {
    button: button,
    buttonBackground: buttonBackground,
    currentButtonType: 'normal',
    buttonLongMin : 120,
    buttonLongMax : 350,
    ButtonLong : 120,
    handle : null,
  },

  ready: function () {
    // console.log(this.data.buttonEvent,)
  },

  methods: {

    /**
     * 按下按钮开始录音
     */
    streamRecord(e) {
      if(this.data.buttonDisabled) {
        return
      }
      // 先清空背景音
      wx.stopBackgroundAudio()

      let currentButtonConf = e.currentTarget.dataset.conf

      this.changeButtonType('press', currentButtonConf.lang)

      this.triggerEvent('recordstart', {
        buttonItem: currentButtonConf
      })

      if (this.data.handle == null) {
        var handle = setInterval(() => {
          var long = this.data.buttonLong
          if (long == null) {
            long = this.data.buttonLongMin
          }
          if (long < this.data.buttonLongMax) {
            long = long + 15;
          }
          this.setData({
            buttonLong: long
          })
        }, 20)

        this.setData({
          handle: handle
        })
      }

     
    },

    /**
     * 松开按钮结束录音
     */
    endStreamRecord(e) {
      let currentButtonConf = e.currentTarget.dataset.conf
      console.log("currentButtonConf", currentButtonConf)

      this.triggerEvent('recordend', {
        buttonItem: currentButtonConf
      })

      if (this.data.handle != null) {
        clearInterval(this.data.handle)
        this.setData({
          handle: null
        })
      }
      

      this.setData({
        buttonLong: this.data.buttonLongMin
      })
    },

    addArticle(e) {
      
      this.triggerEvent('addarticle', {
        
      })

    },


    /**
     * 修改按钮样式
     */
    changeButtonType(buttonType, buttonLang) {

      let tmpButton = this.data.button

      
      if (!buttonLang || buttonLang == tmpButton.lang) {
        tmpButton.buttonType = buttonType
      }

      this.setData({
        button: tmpButton,
      })
    },
  }
});