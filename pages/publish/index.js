//index.js
var util = require('../../utils/util.js')
import utils from '../../utils/index'

Page({
    data: {
        userInfo: {},
        logged: false,
        takeSession: false,
        requestResult: '',
        imgUrl:null,
        publishArticleList : []
    },

    getPublishArticleList : function() {
      wx.showNavigationBarLoading()

      utils.getPublishArticleList((res) => {


        var publishArticleList = res.data.objects

        this.setData({
          publishArticleList: publishArticleList
        })

        wx.hideNavigationBarLoading()
      })
    },

    onShow : function() {

      this.getPublishArticleList()

    },

    onLoad(options) {

      wx.BaaS.login().then(() => {
        this.setData({
          userInfo: wx.BaaS.storage.get('userinfo') ,
          logged:true
        })
      })

      this.getPublishArticleList()

    },

    onTap : function(e) {

      wx.navigateTo({
        url: '/pages/forkdetail/index?article_id=' + e.currentTarget.id
      })

    },

    onLogin : function(data) {
      wx.BaaS.handleUserInfo(data).then(res => {
        this.setData({
          userInfo: wx.BaaS.storage.get('userinfo'),
          logged: true
        })

      }, res => {
      })
    },

    onLogout : function() {
      wx.BaaS.logout().then(res => {
        // success
        this.setData({
          userInfo: {},
          logged: false
        })
      }, err => {
        // err
      })
    },


 
})
